lock-pc-utility is a Java application that uses libusb to read the status of
Caribou charger over USB. This application can be used on various OSes - Linux,
Windows and OSx

How to build for Linux
-----------------------
1. Open the project in eclipse.
2. Right click on the project and click on "Properties".
3. Select "Java Build Path" and go to "Libraries" tab.
4. Click on "Add JARs..." and traverse to "lock-pc-utility --> libs".
5. Select "libusb4java-1.2.0-linux-x86_64.jar" and click "ok".
6. Clean the project and rebuild.
7. Export the runnable JAR.

How to execute on Linux
------------------------
1. Power on the charger and connect to the PC via USB.
2. Make sure that you have Java Run Time 1.6 or above.
3. Copy the JAR to a folder.
4. "cd" to the folder and execute following commad:
   $ java -jar <name_of_the_jar.jar>

How to build for Windows
-------------------------
1. Open the project in eclipse.
2. Right click on the project and click on "Properties".
3. Select "Java Build Path" and go to "Libraries" tab.
4. Click on "Add JARs..." and traverse to "lock-pc-utility --> libs".
5. Select "libusb4java-1.2.0-windows-x86.jar" and click "ok". Please note that
   even for 64 bit Windows, this the JAR to be included.
6. Clean the project and rebuild.
7. Export the runnable JAR.'

How to execute on Windows
--------------------------
1. Power on the charger and connect to PC via USB. The PC should recognize the
   device as "Cariboutech Multicharger" and install the driver for HUB and HID.
2. Download the Zadig utility from http://zadig.akeo.ie/ (install for Windows
   Vista and higher).
3. Open the utility. Go to "Options" and check "List all devices".
4. Select "Cariboutech Multicharger" from the drop down.
5. It should show "HIDUsb" in the "Driver" text box. On the right side text box
   select "WinUSB".
6. Click on "Replace Driver" to replace HID driver with WinUSB driver. It is
   required for libusb to communicate with the device.
7. Copy the JAR to a folder.
8. "cd" to the folder and execute following commad:
   java -jar <name_of_the_jar.jar>
