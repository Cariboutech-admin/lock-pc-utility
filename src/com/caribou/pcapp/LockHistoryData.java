package com.caribou.pcapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LockHistoryData {
    private String[] mEvents = { "", "Lock", "Unlock", "Add New User 22222",
            "Delete User 22222" };

    public String getDateTime() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
                .format(Calendar.getInstance().getTime());
    }

    public String getPasscode() {
        return LockUtils.getRandomPasscode(1, 5, 1, 5);
    }

    public String getEvent() {
        return mEvents[LockUtils.getRandomValue(4, 1)];
    }
}
