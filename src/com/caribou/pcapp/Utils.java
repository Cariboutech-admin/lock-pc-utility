package com.caribou.pcapp;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Utils {
    public static float roundTo2(float value) {
        return Math.round(value * 100) / 100.0f;
    }

    public static ArrayList<ChargerData> reverseList(ArrayList<ChargerData> list) {
        ArrayList<ChargerData> reverseList = new ArrayList<ChargerData>(
                list.size());
        for (int i = list.size() - 1; i >= 0; i--) {
            reverseList.add(list.get(i));
        }
        return reverseList;
    }

    public static byte[] getBytesFromByteBuffer(ByteBuffer buff) {
        byte[] data = new byte[buff.capacity()];
        for (int i = 0; i < buff.capacity(); i++) {
            data[i] = buff.get(i);
        }

        return data;
    }

    public static byte[] getBytesFromByteBuffer(ByteBuffer buff, int offset,
            int length) {
        byte[] data = new byte[length];
        for (int i = 0; i < length; i++) {
            data[i] = buff.get(i + offset);
        }

        return data;
    }

    public static void dumpRtc(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] - 48 + "\t");
        }
        System.out.println();
    }
}
