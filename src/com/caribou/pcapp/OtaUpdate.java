package com.caribou.pcapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class OtaUpdate {

    private static byte SETUP_MAIN_IMAGE_UPDATE = 0x1;
    private static byte SETUP_BOOTLOADER_UPDATE = 0x4;

    private static byte USB_CMD_SETUP = (byte) 0xE0;
    private static byte USB_CMD_XFER_IMAGE = (byte) 0xE1;
    private static byte USB_CMD_END_OTA = (byte) 0xE2;

    public static boolean doBootloaderUpdate(File bootloaderImage) {
        boolean ret = false;
        System.out.println("Set up for updating BL");
        ret = setupOta(SETUP_BOOTLOADER_UPDATE);
        if (ret) {
            System.out.println("Transfer BL image");
            ret = transferImage(bootloaderImage);
            System.out.println("End BL update");
            endOta(SETUP_BOOTLOADER_UPDATE);
        }

        System.out.println("BL update result: " + ret);
        return ret;
    }

    public static boolean doMainImageUpdate(File mainImage) {
        boolean ret = false;
        System.out.println("Set up for updating Main Image");
        ret = setupOta(SETUP_MAIN_IMAGE_UPDATE);
        if (ret) {
            System.out.println("Transfer Main image");
            ret = transferImage(mainImage);
            System.out.println("End Main Image update");
            endOta(SETUP_MAIN_IMAGE_UPDATE);
        }

        System.out.println("Main Image update result: " + ret);
        return ret;
    }

    private static boolean endOta(byte updateArea) {
        return sendUsbCmd(USB_CMD_END_OTA, updateArea);

    }

    private static boolean transferImage(File imageFile) {
        boolean ret = false;
        BufferedReader reader = null;
        // For Java 7 and upwards, we should use try with resources statement
        // instead.
        try {
            reader = new BufferedReader(new FileReader(imageFile));
            String line;
            while ((line = reader.readLine()) != null) {
                ret = parseAndSendImageData(line);
                if (!ret) {
                    break;
                }
            }
        } catch (Exception e) {
            System.err.println("Error while reading image data: "
                    + e.getMessage());
            e.printStackTrace();
            ret = false;
        } finally {
            // Preventing resource leak.
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    /*
     * Hex line format: :BBAAAATT[DDDDDDDD]CC
     * 
     * where : is start of line marker BB is number of data bytes on line AAAA
     * is address in bytes TT is type. 00 means data, 01 means EOF and 04 means
     * extended address DD is data bytes, number depends on BB value CC is
     * checksum (2s-complement of number of bytes+address+data)
     */
    private static boolean parseAndSendImageData(String line) {
        boolean ret = true;
        byte[] buf = line.getBytes();
        int len = buf.length;
        byte[] buf1 = new byte[24]; // Looks like 24 is max number of bytes per
                                    // line

        int i, j;

        if (buf[0] != ':') {
            return false;
        }
        // Hex ascii to int conversion
        // Converts '1' (0x31) to '9' (0x39) to 0x01 to 0x09,
        // Converts 'A' (0x41) to 'F' (0x46) to 0x0A to 0x0F and
        // reduce 2 bytes to 1 byte i.e. '1''0' to 0x10
        for (i = 1, j = 0; i < len; i += 2, j++) {
            if (buf[i] >= 0x30 && buf[i] <= 0x39) {
                buf1[j] = (byte) ((buf[i] - 0x30) << 4);
            } else if (buf[i] >= 0x41 && buf[i] <= 0x46) {
                buf1[j] = (byte) ((buf[i] - 0x37) << 4);
            }
            if (buf[i + 1] >= 0x30 && buf[i + 1] <= 0x39) {
                buf1[j] |= (buf[i + 1] - 0x30);
            } else if (buf[i + 1] >= 0x41 && buf[i + 1] <= 0x46) {
                buf1[j] |= (buf[i + 1] - 0x37);
            }
        }
        // Return if the length of data is null
        if (buf1[0] == 0)
            return ret;
        // Ignore writes to Config area
        // System.err.format("0x%X",buf1[1]);
        if ((buf1[1] & 0xFF) == 0xFF)
            return ret;
        // Ignore lines that are not data (T!=0)
        if (buf1[3] != 0)
            return ret;

        byte[] data = new byte[buf1[0] + 4];

        data[0] = USB_CMD_XFER_IMAGE;
        // Length of data
        data[1] = buf1[0];
        // Address high
        data[2] = buf1[1];
        // Address low
        data[3] = buf1[2];
        // Data:
        System.err.format("0x1%X%X:%d:", buf1[1], buf1[2], buf1[0]);
        for (i = 0; i < buf1[0]; i++) {
            data[4 + i] = buf1[4 + i];
            System.err.format("%X ", buf1[4 + i]);
        }
        System.out.println();

        return UsbDeviceManager.write(data);
    }

    private static boolean setupOta(byte updateArea) {
        return sendUsbCmd(USB_CMD_SETUP, updateArea);
    }

    private static boolean sendUsbCmd(byte cmd, byte updateArea) {
        byte[] data = new byte[2];
        data[0] = cmd;
        data[1] = updateArea;
        return UsbDeviceManager.write(data);
    }
}
