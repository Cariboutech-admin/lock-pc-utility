package com.caribou.pcapp;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class ChargerDataProvider {

    private static final int MAX_NUM_OF_PORTS = 10;
    private static final short VID = 0x4D8;
    private static final short PID = 0x3F;
    private static final int CMD_SIZE = 1;
    private static final byte CMD_READ_PORTS_DATA = (byte) 0x80;
    private static final int DATA_PER_PORT = 24;
    private static final int MAX_DATA_READ_PER_TRANSACTION = 64;
    private static final int READ_DATA_SIZE = DATA_PER_PORT * MAX_NUM_OF_PORTS;
    private static final byte CMD_READ_RTC = (byte) 0x82;
    private static final byte CMD_READ_CHARGE_COMPLETION_RTC = (byte) 0x85;
    private static final int CMD_READ_RTC_DATA_SIZE = 14;
    // Data offsets
    private static final int OFFSET_INTERNAL_PORT = 0;
    private static final int OFFSET_EXTERNAL_PORT = 1;
    private static final int OFFSET_STATUS = 2;
    private static final int OFFSET_CHARGING_COMPLETE = 3;
    private static final int OFFSET_VOLTAGE = 4;
    private static final int OFFSET_CURRENT = 6;
    private static final int OFFSET_PROFILE = 8;
    private static final int OFFSET_BASE_RTC = 10;

    private static final boolean MOCKED = false;
    public static final boolean DEBUG = true;

    /**
     * Initializes the Charger data provider. Should be called by the client
     * before making any request for charger data.
     * 
     * @return true if success, else false.
     */
    public static boolean init() {
        return UsbDeviceManager.init();
    }

    /**
     * Exits the Charger data provider. Should be called by the client before
     * terminating to release the acquired resources.
     */
    public static void exit() {
        UsbDeviceManager.exit();
    }

    /**
     * Gets the data for all the ports of the given charging station. Currently
     * the stationNumber is redundant since we support only one station.
     * 
     * @param stationNumber
     *            the station number.
     * @return List of data for all the ports on the charging station. Returns
     *         null if no data is found.
     */
    public static ArrayList<ChargerData> getChargerData(int stationNumber) {
        ArrayList<ChargerData> dataList = new ArrayList<ChargerData>();
        if (UsbDeviceManager.open(VID, PID)) {
            ByteBuffer dataBuff = readData();
            System.out.println("dataBuff read.");
            if (dataBuff != null) {
                System.out.println("dataBuff read. Size: "
                        + dataBuff.capacity());
                dataList = getDataFromBuffer(dataBuff);
                dataList = Utils.reverseList(dataList);
            }
            UsbDeviceManager.close();
        } else if (MOCKED) {
            dataList = MockChargerData.getMockData();
        }
        return dataList;
    }

    /**
     * Gets the data for the given port of the given charging station. The input
     * port number should be '1' based. Currently the stationNumber is redundant
     * since we support only one station.
     * 
     * @param stationNumber
     *            the station number.
     * @param portNumber
     *            the port number.
     * @return data for the given port. Returns null if no data is found.
     */
    public static ChargerData getPortData(int stationNumber, int portNumber) {
        ArrayList<ChargerData> dataList = getChargerData(stationNumber);
        if (dataList != null && portNumber > 0
                && portNumber <= MAX_NUM_OF_PORTS) {
            return dataList.get(portNumber - 1);
        }

        if (portNumber > MAX_NUM_OF_PORTS || portNumber <= 0) {
            System.err.println("Invalid port number. Should be between 1 and "
                    + MAX_NUM_OF_PORTS);
        }
        return null;
    }

    /**
     * Sets the charging profile to the given value for the given port. Any
     * value between 0 and 7 (both inclusive) indicates Auto mode. Any value
     * between 8 and 15 (both inclusive) indicate profiles 0 to 7 in manual
     * mode.
     * 
     * @param portNum
     *            The port number (between 1 and 10)
     * @param profile
     *            The profile (between 0 and 15)
     */
    public static void setProfile(int portNum, int profile) {
        byte[] data = new byte[3];
        data[0] = (byte) 0x83;
        data[1] = (byte) (portNum & 0xff);
        data[2] = (byte) (profile & 0xff);

        System.out.println("port: " + data[1]);
        System.out.println("profile: " + data[2]);

        if (data[1] < 1 || data[1] > MAX_NUM_OF_PORTS) {
            System.err.println("[main]: Invalid port.");
            return;
        }

        if (data[2] < 0 || data[2] > 16) {
            System.err.println("[main]: Invalid profile.");
            return;
        }
        if (UsbDeviceManager.open(VID, PID)) {
            UsbDeviceManager.write(data);
            UsbDeviceManager.close();
        }
    }

    public static boolean doBootloaderUpdate(File bootloaderImage) {
        boolean ret = false;
        if (UsbDeviceManager.open(VID, PID)) {
            ret = OtaUpdate.doBootloaderUpdate(bootloaderImage);
            UsbDeviceManager.close();
        }

        return ret;
    }

    public static boolean doMainImageUpdate(File mainImage) {
        boolean ret = false;
        if (UsbDeviceManager.open(VID, PID)) {
            ret = OtaUpdate.doMainImageUpdate(mainImage);
            UsbDeviceManager.close();
        }

        return ret;
    }

    private static ByteBuffer readData() {
        byte[] data = new byte[CMD_SIZE];
        data[0] = CMD_READ_PORTS_DATA;

        if (!UsbDeviceManager.write(data)) {
            return null;
        }

        ByteBuffer chargerData = ByteBuffer.allocate(READ_DATA_SIZE);
        int bytesToRead = READ_DATA_SIZE;
        while (bytesToRead >= MAX_DATA_READ_PER_TRANSACTION) {
            ByteBuffer temp = UsbDeviceManager
                    .read(MAX_DATA_READ_PER_TRANSACTION);
            if (temp == null) {
                return temp;
            }
            chargerData.put(temp);
            bytesToRead -= MAX_DATA_READ_PER_TRANSACTION;
        }
        if (bytesToRead > 0) {
            ByteBuffer temp = UsbDeviceManager.read(bytesToRead);
            if (temp == null) {
                return temp;
            }
            chargerData.put(temp);
        }

        return chargerData;
    }

    private static ArrayList<ChargerData> getDataFromBuffer(ByteBuffer buff) {
        ArrayList<ChargerData> dataList = new ArrayList<ChargerData>(
                MAX_NUM_OF_PORTS);
        int idx;
        byte internalPort;
        byte externalPort;
        byte status;
        byte chargingComplete;
        int voltage;
        float fVoltage;
        int current;
        float fCurrent;
        int profile;
        byte[] baseRtc = new byte[CMD_READ_RTC_DATA_SIZE];
        byte[] completionRtc = new byte[CMD_READ_RTC_DATA_SIZE];
        byte[] currentRtc = readCurrentRtc();

        for (int i = 0; i < MAX_NUM_OF_PORTS; i++) {
            idx = i * DATA_PER_PORT;
            internalPort = buff.get(idx + OFFSET_INTERNAL_PORT);
            externalPort = buff.get(idx + OFFSET_EXTERNAL_PORT);
            status = buff.get(idx + OFFSET_STATUS);
            chargingComplete = buff.get(idx + OFFSET_CHARGING_COMPLETE);

            voltage = ((buff.get(idx + OFFSET_VOLTAGE + 1) << 8) | buff.get(idx
                    + OFFSET_VOLTAGE));

            // Convert current to unsigned short
            int b1 = buff.get(idx + OFFSET_CURRENT + 1) & 0xff;
            int b2 = buff.get(idx + OFFSET_CURRENT) & 0xff;
            current = (b1 << 8) | b2;

            profile = ((buff.get(idx + OFFSET_PROFILE + 1) << 8) | buff.get(idx
                    + OFFSET_PROFILE));

            baseRtc = Utils.getBytesFromByteBuffer(buff,
                    (idx + OFFSET_BASE_RTC), CMD_READ_RTC_DATA_SIZE);

            // Calculate float value of voltage and current.
            fVoltage = (float) ((voltage * 3.3 * 2) / 1024);
            fVoltage = Utils.roundTo2(fVoltage);
            fCurrent = (float) ((current * 3.3 * 1000) / (1024 * 0.033 * 20));
            fCurrent = Utils.roundTo2(fCurrent);

            ChargingStatus chargingStatus = ChargingStatus.convert(status);

            // Check if calibrating
            if (profile == -2) {
                chargingStatus = ChargingStatus.CALIBRATING;
            }

            String timeOnChargeInSeconds = "0s";
            if (chargingStatus == ChargingStatus.CONNECTED) {
                timeOnChargeInSeconds = TimeOnChargeHelper.getTimeOnCharge(
                        baseRtc, currentRtc);
            }

            String timeToFullCharge = "";
            // Check if charging is complete
            if (chargingComplete == 10) {
                chargingStatus = ChargingStatus.COMPLETE;
                completionRtc = readChargeCompletionRtc(internalPort);
                timeToFullCharge = TimeOnChargeHelper.getTimeOnCharge(baseRtc,
                        completionRtc);
            }

            ChargerData chargerData = new ChargerData(externalPort,
                    timeOnChargeInSeconds, chargingStatus, fVoltage, fCurrent,
                    profile, timeToFullCharge);
            dataList.add(chargerData);
        }

        return dataList;
    }

    private static byte[] readCurrentRtc() {
        byte[] data = new byte[CMD_SIZE];
        data[0] = CMD_READ_RTC;

        if (!UsbDeviceManager.write(data)) {
            return null;
        }

        ByteBuffer rtcData = ByteBuffer.allocate(CMD_READ_RTC_DATA_SIZE);
        rtcData = UsbDeviceManager.read(CMD_READ_RTC_DATA_SIZE);
        System.out.println("Read current RTC data");

        return Utils.getBytesFromByteBuffer(rtcData);
    }

    private static byte[] readChargeCompletionRtc(byte port) {
        byte[] data = new byte[2];
        data[0] = CMD_READ_CHARGE_COMPLETION_RTC;
        data[1] = port;

        if (!UsbDeviceManager.write(data)) {
            return null;
        }

        ByteBuffer rtcData = ByteBuffer.allocate(CMD_READ_RTC_DATA_SIZE);
        rtcData = UsbDeviceManager.read(CMD_READ_RTC_DATA_SIZE);
        System.out.println("Read charge completion RTC data for port: "
                + (10 - port));

        return Utils.getBytesFromByteBuffer(rtcData);
    }
}
