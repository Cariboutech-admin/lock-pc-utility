package com.caribou.pcapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LockDataProvider {
    private static String mLockTypes[] = { "", "FM", "SM", "LO" };
    private static String mLockType;

    public static LockHistoryData getLockHistory() {
        return new LockHistoryData();
    }

    public static LockUserData getUsers() {
        return new LockUserData();
    }

    public static String getLockSerialNumber() {
        String val = "Wesko Lock ";
        mLockType = mLockTypes[LockUtils.getRandomValue(3, 1)];
        val += mLockType;
        val += LockUtils.getRandomValue(255, 1);
        return val;
    }

    public static int getMicrochipFWVersion() {
        return LockUtils.getRandomValue(255, 1);
    }

    public static String getBleFWVersion() {
        return "Dummy BLE Version";
    }

    public static String getHWRevision() {
        String val = mLockType;
        val += LockUtils.getRandomValue(10, 1);
        return val;
    }

    public static String getManufactureDate() {
        return new SimpleDateFormat("dd-MM-yyyy")
                .format(Calendar.getInstance().getTime());
    }
}
