package com.caribou.pcapp;

import java.util.ArrayList;
import java.util.Random;

public class MockChargerData {
    private static final int MAX_NUM_OF_PORTS = 10;

    public static ArrayList<ChargerData> getMockData() {
        ArrayList<ChargerData> mockDataList = new ArrayList<ChargerData>(
                MAX_NUM_OF_PORTS);
        float current = 0.0f;
        ChargingStatus chargingStatus;
        for (int i = 0; i < MAX_NUM_OF_PORTS; i++) {
            current = getRandomCurrent();
            if (current <= 90.0f) {
                chargingStatus = ChargingStatus.COMPLETE;
            } else {
                chargingStatus = getMockChargingStatus();
            }
            ChargerData mockData = new ChargerData(i + 1, "", chargingStatus,
                    getRandomVoltage(), current, getRandomChargingProfile(), "");
            mockDataList.add(mockData);
        }

        return mockDataList;
    }

    private static ChargingStatus getMockChargingStatus() {
        int seed = (int) Math.round(Math.random()
                * (ChargingStatus.getMax() - ChargingStatus.getMin()));
        return ChargingStatus.convert((byte) seed);
    }

    private static float getRandomVoltage() {
        return Utils.roundTo2((float) (Math.random() * 5));
    }

    private static float getRandomCurrent() {
        return Utils.roundTo2((float) (Math.random() * 2000));
    }

    private static int getRandomChargingProfile() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(16);
    }
}
