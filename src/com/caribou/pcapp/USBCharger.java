package com.caribou.pcapp;

import java.util.ArrayList;

public class USBCharger {

    private static void dumpDataFromDataList(ArrayList<ChargerData> dataList) {
        for (ChargerData chargerData : dataList) {
            System.out.println("external port: " + chargerData.getPortNumber());
            System.out.println(
                    "status: " + chargerData.getChargingStatus().name());
            System.out.println("voltage: " + chargerData.getVoltage() + " V");
            System.out.println("current: " + chargerData.getCurrent() + " mA");
            System.out.println(
                    "time on charge: " + chargerData.getTimeOnCharge());
            System.out.println("profile: " + chargerData.getChargingProfile());
            System.out.println(
                    "-------------------------------------------------------------------------------");
            System.out.println();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // ChargerDataProvider.init();
        // ArrayList<ChargerData> dataList =
        // ChargerDataProvider.getChargerData(0);
        //
        // if (args.length > 0) {
        // System.out.println("args[0]: " + args[0]);
        // System.out.println("args[1]: " + args[1]);
        //
        // int port = Integer.parseInt(args[0]);
        // int profile = Integer.parseInt(args[1]);
        //
        // ChargerDataProvider.setProfile(port, profile);
        // }
        //
        // if (dataList != null) {
        // dumpDataFromDataList(dataList);
        // } else {
        // System.err.println("[main]: Test device not found.");
        // }
        // ChargerDataProvider.exit();
        System.out.println("========== HISTORY ==========");
        for (int i = 0; i < 10; i++) {
            LockHistoryData lockHistoryData = LockDataProvider.getLockHistory();
            System.out.println("date time = " + lockHistoryData.getDateTime());
            System.out.println("passcode = " + lockHistoryData.getPasscode());
            System.out.println("Event = " + lockHistoryData.getEvent() + "\n");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        System.out.println("========== USERS ==========");
        for (int i = 0; i < 10; i++) {
            LockUserData lockUserData = LockDataProvider.getUsers();
            System.out.println("passcode = " + lockUserData.getPasscode());
            System.out.println("is admin = " + lockUserData.isAdmin() + "\n");
        }

        System.out.println("==============================");
        System.out.println("Lock serial number = "
                + LockDataProvider.getLockSerialNumber());
        System.out.println("Microchip firmware version = "
                + LockDataProvider.getMicrochipFWVersion());
        System.out.println(
                "BLE firmware version = " + LockDataProvider.getBleFWVersion());
        System.out.println(
                "Hardware revision = " + LockDataProvider.getHWRevision());
        System.out.println("Date of manufacture = "
                + LockDataProvider.getManufactureDate());
    }
}
