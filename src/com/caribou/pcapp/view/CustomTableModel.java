package com.caribou.pcapp.view;

import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.caribou.pcapp.ChargerDataProvider;

public class CustomTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private List<Object[]> mValues;
    private String[] mProfiles;
    private Object[][] mData;
    private static final boolean DEBUG = true;
    private String[] columnNames = { StringUtils.tableHeader0.toString(),
            StringUtils.tableHeader1.toString(),
            StringUtils.tableHeader2.toString(),
            StringUtils.tableHeader3.toString(),
            StringUtils.tableHeader4.toString(),
            StringUtils.tableHeader5.toString(),
            StringUtils.tableHeader6.toString() };

    public CustomTableModel(List<Object[]> values, String[] profiles) {
        super();
        this.mValues = values;
        this.mProfiles = profiles;
        this.mData = mValues.toArray(new Object[][] {});
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return mData.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        return mData[row][col];
    }

    /*
     * JTable uses this method to determine the default renderer/ editor for
     * each cell.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /*
     * Don't need to implement this method unless your table's editable.
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (StringUtils.tableHeader6.toString().equals(columnNames[col])) {
            // Don't trigger update for same value of profile
            if (value != getValueAt(row, col)) {
                if (DEBUG) {
                    if (!value.equals("")) {
                        System.out.println("trigger update for row: " + row
                                + " to " + value);
                    }
                }
                if (!value.equals("")) {
                    mData[row][col] = value;
                    setProfile(row + 1, Arrays.asList(mProfiles).indexOf(value));

                }
                fireTableCellUpdated(row, col);
            }
        } else {
            mData[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }

    private void setProfile(int port, int profile) {
        // profile values 0 to 7 represent Auto set profiles.
        // profile values 8 to 15 represent Manually set profiles.
        // Adding 7 since profile 8 corresponds to mProfiles[1] and so
        // on so forth.
        if (port < 0 || port > 10 || profile < 0 || profile > 10) {
            return;
        }
        ChargerDataProvider.setProfile((port), (profile + 7));
    }

    @SuppressWarnings("unused")
    private void printDebugData() {
        int numRows = getRowCount();
        int numCols = getColumnCount();

        for (int i = 0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j = 0; j < numCols; j++) {
                System.out.print("  " + mData[i][j]);
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }

}
