package com.caribou.pcapp.view;

import java.awt.Color;

public class ColorUtils {
    public static final Color WHITE_TABLE_ROW_COLOR = new Color(217, 217, 217);
    public static final Color ALTERNATE_TABLE_ROW_COLOR = new Color(250, 250,
            250);
    public static final Color CARIBOU_GREEN = new Color(149, 186, 61);
    public static final Color CARIBOU_BLUE = new Color(84, 144, 204);
    public static final Color CANVAS_BACKGROUND = new Color(219, 255, 255);
    public static final Color TAB_SELECTED = new Color(84, 144, 204);
    public static final Color CHARGING_STATUS_COMPLETE = new Color(5, 135, 59);
    public static final Color FAILED_UPDATE = new Color(218, 106, 69);
}
