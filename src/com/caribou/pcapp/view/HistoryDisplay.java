package com.caribou.pcapp.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.caribou.pcapp.LockDataProvider;
import com.caribou.pcapp.LockHistoryData;
import com.opencsv.CSVWriter;


@SuppressWarnings("serial")
public class HistoryDisplay extends JPanel implements ActionListener{
	private static final String COLUMN_DATE = "Date&Time";
	private static final String COLUMN_PASSCODE = "Passcode";
	private static final String COLUMN_EVENT = "Event";
	private static final String DOWNLOAD_HISTORY = "Download History";
	private static final String CSV_FILE_NAME = "history.csv";
	private static final String PRINT_HISTORY = "Print History";
	private List<String[]> dataForCsv = new ArrayList<String[]>();

	public HistoryDisplay() {
		super(new GridLayout(1, 0));

		JTable table = new JTable(new HistoryTableModel());
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setRowHeight(20);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		table.setDefaultRenderer(String.class, centerRenderer);
		createPopupMenu(table);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel..
		add(scrollPane);
	}

	class HistoryTableModel extends AbstractTableModel {
		private String[] columnNames = { COLUMN_DATE, COLUMN_PASSCODE,
				COLUMN_EVENT };
		
		private Object[][] data = getHistoryData(columnNames.length);

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.length;
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		/*
		 * JTable uses this method to determine the default renderer/ editor for
		 * each cell. If we didn't implement this method, then the last column
		 * would contain text ("true"/"false"), rather than a check box.
		 */
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		/*
		 * Don't need to implement this method unless your table's editable.
		 */
		public boolean isCellEditable(int row, int col) {
			// Note that the data/cell address is constant,
			// no matter where the cell appears onscreen.
			if (col < 2) {
				return false;
			} else {
				return true;
			}
		}

		private Object[][] getHistoryData(int columCount) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			LockHistoryData lockHistoryData = LockDataProvider.getLockHistory();
			// put numRows = 10 for the time being. It should be
			// lockHistory.length();
			
			int numRows = 10;
			Object data[][] = new Object[numRows][columCount];
			for (int i = 0; i < 10; i++) {
				data[i][0] = lockHistoryData.getDateTime();
				data[i][1] = lockHistoryData.getPasscode();
				data[i][2] = lockHistoryData.getEvent();
				dataForCsv.add(new String[]{lockHistoryData.getDateTime(), lockHistoryData.getPasscode(), lockHistoryData.getEvent()});
			}
			// data[row][col] = value;
			// Normally, one should call fireTableCellUpdated() when
			// a value is changed. However, doing so in this demo
			// causes a problem with TableSorter. The tableChanged()
			// call on TableSorter that results from calling
			// fireTableCellUpdated() causes the indices to be regenerated
			// when they shouldn't be. Ideally, TableSorter should be
			// given a more intelligent tableChanged() implementation,
			// and then the following line can be uncommented.
			// fireTableCellUpdated(row, col);
			return data;
		}

		private void printDebugData() {
			int numRows = getRowCount();
			int numCols = getColumnCount();

			for (int i = 0; i < numRows; i++) {
				System.out.print("    row " + i + ":");
				for (int j = 0; j < numCols; j++) {
					System.out.print("  " + data[i][j]);
				}
				System.out.println();
			}
			System.out.println("--------------------------");
		}
	}
	
	private void createPopupMenu(JComponent component) {
		JMenuItem menuItem;

		// Create the popup menu.
		JPopupMenu popup = new JPopupMenu();
		menuItem = new JMenuItem(DOWNLOAD_HISTORY);
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				writeToCsv();				
			}
		});
		popup.add(menuItem);
//		menuItem = new JMenuItem(PRINT_HISTORY);
//		menuItem.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				//perform print functionality here				
//			}
//		});
//		popup.add(menuItem);

		// Add listener to the text area so the popup menu can come up.
		MouseListener popupListener = new PopupListener(popup);
		component.addMouseListener(popupListener);
	}

	class PopupListener extends MouseAdapter {
		JPopupMenu popup;

		PopupListener(JPopupMenu popupMenu) {
			popup = popupMenu;
		}

		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}
	
	private void writeToCsv(){
		//create a file browser
		JFileChooser chooser = new JFileChooser();
		File selectedFile = new File(CSV_FILE_NAME);
		chooser.setSelectedFile(selectedFile);
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int option = chooser.showSaveDialog(null);
		if(JFileChooser.APPROVE_OPTION == option){
			//save it to the directory
			File file = new File(chooser.getSelectedFile().getAbsolutePath());
			try {						
				 StringWriter sw = new StringWriter();
			     CSVWriter writer = new CSVWriter(new FileWriter(file));
			     writer.writeAll(dataForCsv);
			     writer.flush();
			     writer.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	
}
