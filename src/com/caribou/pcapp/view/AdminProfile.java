package com.caribou.pcapp.view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.caribou.pcapp.ChargerDataProvider;
import com.caribou.pcapp.LockDataProvider;

@SuppressWarnings("serial")
public class AdminProfile extends JPanel implements ActionListener {

	private JFileChooser mFileChooser;
	private FileFilter mFilter;
	private static final long BOOT_LOADER_FILE_SIZE_MAX = 3000l;
	private static final long IMAGE_FILE_SIZE_MAX = 70000l;
	private JProgressBar mProgressBarBootLoader;
	private JProgressBar mProgressBarImage;
	private JTextField mTextFieldBootLoader;
	private JTextField mTextFieldImage;
	private File mBootLoaderFile, mImageFile;
	private final String SUCCESS_MESSAGE = "Successfully updated";
	private final String FAILURE_MESSAGE = "Updation failed";
	private final String UPDATING_IMAGELOADER_MESSAGE = "Updating ImageLoader";
	private final String UPDATING_BOOTLOADER = "Updating Bootloader";
	private final String CONFIGURATION_TITLE = "Lock Configuration";
	private final String LOCK_STATISTICS_TITLE = "Lock Statistics";
	private final String LOCK_SERIAL_NUMBER_LBL = "Lock Serial Number";
	private final String BLE_VERSION_LBL = "<html>BLE Firmware <br> Version</html>";
	private final String LOCK_FIRMWARE_VERSION_LBL = "<html>Lock Firmware <br> Version</html>";
	private final String BATTERY_CHARGE_DISCHARGE_OPS_LBL = "<html>Battery charge /<br>discharge ops</html>";
	private final String LOCK_UNLOCK_SINCE_LAST_CHARGE_LBL = "<html>Unlock / Lock ops<br>since last charge</html>";
	private final String TOTAL_UNLOCK_LOCK_OPS_LBL = "<html>Total Unlock/lock<br>ops over lifetime</html>";
	private final String DATE_MANUFACTURED_LBL = "Date Manufactured";
	private final String LOCK_TYPE_LBL = "Lock Type";
	private final String LAST_CHARGE_DATE_LBL = "Last Charge Date";
	private JPanel panel_2;
	private JTextField textField_1;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;

	/**
	 * Create the panel.
	 */

	public AdminProfile() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLayout(new GridLayout(0, 2, 0, 0));
		setBounds(100, 100, 880, 440);

		mFileChooser = new JFileChooser();
		mFilter = new FileNameExtensionFilter("hex", "hex");
		mFileChooser.addChoosableFileFilter(mFilter);
		mFileChooser.setAcceptAllFileFilterUsed(false);
		mFileChooser.setMultiSelectionEnabled(false);

		JPanel panelFirmware = new JPanel();
		panelFirmware.setBorder(new CompoundBorder(new TitledBorder(
				new LineBorder(new Color(184, 207, 229)), "Update Firmware",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				ColorUtils.CARIBOU_BLUE), new BevelBorder(BevelBorder.RAISED,
				new Color(255, 255, 255), null, null, null)));
		add(panelFirmware);
		GridBagLayout gbl_panelFirmware = new GridBagLayout();
		gbl_panelFirmware.columnWidths = new int[] { 30, 192, 30, 0 };
		gbl_panelFirmware.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30,
				30, 0, 0, 30, 30, 30, 30 };
		gbl_panelFirmware.columnWeights = new double[] { 1.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panelFirmware.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		panelFirmware.setLayout(gbl_panelFirmware);

		mProgressBarImage = new JProgressBar();
		mProgressBarImage.setVisible(false);

		mProgressBarBootLoader = new JProgressBar();
		mProgressBarBootLoader.setStringPainted(true);
		mProgressBarBootLoader.setVisible(false);

		JButton btnUpdateFirmware = new JButton("Update");
		btnUpdateFirmware.addActionListener(this);

		JButton btnUpdateVersion = new JButton(
				StringUtils.upload_main_image.toString());
		btnUpdateVersion.addActionListener(this);

		mTextFieldBootLoader = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.BOTH;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panelFirmware.add(mTextFieldBootLoader, gbc_textField_1);
		mTextFieldBootLoader.setColumns(10);

		JButton btnBootLoaderUpdate = new JButton(
				StringUtils.upload_bl_image.toString());
		btnBootLoaderUpdate.addActionListener(this);
		btnBootLoaderUpdate.setBackground(ColorUtils.CARIBOU_GREEN);
		btnBootLoaderUpdate.setForeground(Color.WHITE);
		GridBagConstraints gbc_btnStationToUpdate = new GridBagConstraints();
		gbc_btnStationToUpdate.fill = GridBagConstraints.BOTH;
		gbc_btnStationToUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnStationToUpdate.gridx = 1;
		gbc_btnStationToUpdate.gridy = 3;
		panelFirmware.add(btnBootLoaderUpdate, gbc_btnStationToUpdate);

		mTextFieldImage = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 5;
		panelFirmware.add(mTextFieldImage, gbc_textField);
		mTextFieldImage.setColumns(10);
		btnUpdateVersion.setBackground(ColorUtils.CARIBOU_GREEN);
		btnUpdateVersion.setForeground(Color.WHITE);
		GridBagConstraints gbc_btnUpdateVersion = new GridBagConstraints();
		gbc_btnUpdateVersion.fill = GridBagConstraints.BOTH;
		gbc_btnUpdateVersion.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdateVersion.gridx = 1;
		gbc_btnUpdateVersion.gridy = 6;
		panelFirmware.add(btnUpdateVersion, gbc_btnUpdateVersion);
		btnUpdateFirmware.setBackground(ColorUtils.CARIBOU_BLUE);
		btnUpdateFirmware.setForeground(Color.WHITE);
		GridBagConstraints gbc_btnUpdateFirmware = new GridBagConstraints();
		gbc_btnUpdateFirmware.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdateFirmware.fill = GridBagConstraints.BOTH;
		gbc_btnUpdateFirmware.gridx = 1;
		gbc_btnUpdateFirmware.gridy = 8;
		panelFirmware.add(btnUpdateFirmware, gbc_btnUpdateFirmware);
		GridBagConstraints gbc_progressBar_1 = new GridBagConstraints();

		gbc_progressBar_1.fill = GridBagConstraints.BOTH;
		gbc_progressBar_1.insets = new Insets(0, 0, 5, 5);
		gbc_progressBar_1.gridx = 1;
		gbc_progressBar_1.gridy = 10;
		panelFirmware.add(mProgressBarBootLoader, gbc_progressBar_1);
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.fill = GridBagConstraints.BOTH;
		gbc_progressBar.insets = new Insets(0, 0, 5, 5);
		gbc_progressBar.gridx = 1;
		gbc_progressBar.gridy = 12;
		panelFirmware.add(mProgressBarImage, gbc_progressBar);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.2);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane);

		panel_2 = new JPanel();
		panel_2.setBorder(new CompoundBorder(new TitledBorder(new BevelBorder(
				BevelBorder.LOWERED, null, null, null, null),
				CONFIGURATION_TITLE, TitledBorder.LEADING, TitledBorder.TOP,
				null, ColorUtils.CARIBOU_BLUE), new BevelBorder(
				BevelBorder.RAISED, null, null, null, null)));
		splitPane.setLeftComponent(panel_2);

		addComponentsToConfigPanel(panel_2);

		textField = new JTextField(LockDataProvider.getLockSerialNumber());
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 0;
		panel_2.add(textField, gbc_textField_2);
		textField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel(LOCK_FIRMWARE_VERSION_LBL);
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		panel_2.add(lblNewLabel_1, gbc_lblNewLabel_1);

		textField_1 = new JTextField(LockDataProvider.getMicrochipFWVersion());
		textField_1.setEnabled(false);
		GridBagConstraints gbc_textField_1_1 = new GridBagConstraints();
		gbc_textField_1_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1_1.gridx = 1;
		gbc_textField_1_1.gridy = 1;
		panel_2.add(textField_1, gbc_textField_1_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel(BLE_VERSION_LBL);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel_2.add(lblNewLabel_2, gbc_lblNewLabel_2);

		textField_2 = new JTextField(LockDataProvider.getBleFWVersion());
		textField_2.setEnabled(false);
		GridBagConstraints gbc_textField_2_1 = new GridBagConstraints();
		gbc_textField_2_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2_1.gridx = 1;
		gbc_textField_2_1.gridy = 2;
		panel_2.add(textField_2, gbc_textField_2_1);
		textField_2.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("HW Version");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 3;
		panel_2.add(lblNewLabel_3, gbc_lblNewLabel_3);

		textField_3 = new JTextField(LockDataProvider.getHWRevision());
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 3;
		panel_2.add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);

		JLabel lblDateManufactured = new JLabel(DATE_MANUFACTURED_LBL);
		GridBagConstraints gbc_lblDateManufactured = new GridBagConstraints();
		gbc_lblDateManufactured.anchor = GridBagConstraints.EAST;
		gbc_lblDateManufactured.insets = new Insets(0, 0, 5, 5);
		gbc_lblDateManufactured.gridx = 0;
		gbc_lblDateManufactured.gridy = 4;
		panel_2.add(lblDateManufactured, gbc_lblDateManufactured);

		textField_4 = new JTextField( LockDataProvider.getManufactureDate());
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.insets = new Insets(0, 0, 5, 5);
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.gridx = 1;
		gbc_textField_4.gridy = 4;
		panel_2.add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);

		JLabel lblLockType = new JLabel(LOCK_TYPE_LBL);
		GridBagConstraints gbc_lblLockType = new GridBagConstraints();
		gbc_lblLockType.anchor = GridBagConstraints.EAST;
		gbc_lblLockType.insets = new Insets(0, 0, 0, 5);
		gbc_lblLockType.gridx = 0;
		gbc_lblLockType.gridy = 5;
		panel_2.add(lblLockType, gbc_lblLockType);

		textField_5 = new JTextField();
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.insets = new Insets(0, 0, 0, 5);
		gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_5.gridx = 1;
		gbc_textField_5.gridy = 5;
		panel_2.add(textField_5, gbc_textField_5);
		textField_5.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED,
				null, null, null, null), LOCK_STATISTICS_TITLE,
				TitledBorder.LEADING, TitledBorder.TOP, null,
				ColorUtils.CARIBOU_BLUE));
		splitPane.setRightComponent(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblNewLabel_4 = new JLabel(LAST_CHARGE_DATE_LBL);
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 0;
		panel_1.add(lblNewLabel_4, gbc_lblNewLabel_4);

		textField_6 = new JTextField();
		textField_6.setEnabled(false);
		GridBagConstraints gbc_textField_6 = new GridBagConstraints();
		gbc_textField_6.insets = new Insets(0, 0, 5, 0);
		gbc_textField_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_6.gridx = 3;
		gbc_textField_6.gridy = 0;
		panel_1.add(textField_6, gbc_textField_6);
		textField_6.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel(BATTERY_CHARGE_DISCHARGE_OPS_LBL);
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 1;
		gbc_lblNewLabel_5.gridy = 1;
		panel_1.add(lblNewLabel_5, gbc_lblNewLabel_5);

		textField_7 = new JTextField();
		textField_7.setEnabled(false);
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.insets = new Insets(0, 0, 5, 0);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 3;
		gbc_textField_7.gridy = 1;
		panel_1.add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel(LOCK_UNLOCK_SINCE_LAST_CHARGE_LBL);
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 1;
		gbc_lblNewLabel_6.gridy = 2;
		panel_1.add(lblNewLabel_6, gbc_lblNewLabel_6);

		textField_8 = new JTextField();
		textField_8.setEnabled(false);
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.insets = new Insets(0, 0, 5, 0);
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.gridx = 3;
		gbc_textField_8.gridy = 2;
		panel_1.add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);

		JLabel lblTotalLockUnlock = new JLabel(TOTAL_UNLOCK_LOCK_OPS_LBL);
		GridBagConstraints gbc_lblTotalLockUnlock = new GridBagConstraints();
		gbc_lblTotalLockUnlock.anchor = GridBagConstraints.EAST;
		gbc_lblTotalLockUnlock.insets = new Insets(0, 0, 0, 5);
		gbc_lblTotalLockUnlock.gridx = 1;
		gbc_lblTotalLockUnlock.gridy = 3;
		panel_1.add(lblTotalLockUnlock, gbc_lblTotalLockUnlock);

		textField_9 = new JTextField();
		textField_9.setEnabled(false);
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 3;
		gbc_textField_9.gridy = 3;
		panel_1.add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		final String actionCommand = e.getActionCommand();
		final JPanel parentComponent = this;
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (StringUtils.update_sys_image.toString().equals(
						actionCommand)) {
					// mProgressBarBootLoader.setIndeterminate(true);
					new GUIWorker(actionCommand).execute();

				} else {
					int returnVal = mFileChooser
							.showOpenDialog(parentComponent);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = mFileChooser.getSelectedFile();
						if (StringUtils.upload_bl_image.toString().equals(
								actionCommand)) {
							mTextFieldBootLoader.setText(file.getPath());
							mBootLoaderFile = file;
							if (mProgressBarBootLoader.isShowing()
									&& !mProgressBarBootLoader
											.isIndeterminate()) {
								mProgressBarBootLoader.setVisible(false);
							}
						} else if (StringUtils.upload_main_image.toString()
								.equals(actionCommand)) {
							mTextFieldImage.setText(file.getPath());
							mImageFile = file;
							if (mProgressBarImage.isShowing()
									&& !mProgressBarImage.isIndeterminate()) {
								mProgressBarImage.setVisible(false);
							}
						}
					}
				}

			}

		});

	}

	private void addComponentsToConfigPanel(JPanel panel) {
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 124, 114, 136, 0 };
		gbl_panel_2.rowHeights = new int[] { 19, 19, 0, 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);
		JLabel lblNewLabel = new JLabel(LOCK_SERIAL_NUMBER_LBL);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
	}

	class Progress {
		private String task;
		private boolean complete;

		public Progress(String task, boolean complete) {
			super();
			this.task = task;
			this.complete = complete;
		}

		public String getTask() {
			return task;
		}

		public boolean isComplete() {
			return complete;
		}

	}

	class GUIWorker extends SwingWorker<Boolean, Progress> {
		String action;

		public GUIWorker(String action) {
			this.action = action;
		}

		private boolean isEmpty(String s) {
			return ((s == null) || (s.trim().length() == 0));
		}

		@Override
		protected Boolean doInBackground() throws Exception {
			String bootLoaderFileName = mTextFieldBootLoader.getText();
			String imageLoaderFileName = mTextFieldImage.getText();
			boolean status = false;
			if (!isEmpty(bootLoaderFileName) && !isEmpty(imageLoaderFileName)) {
				// Turn off the auto refresh
				LockTabbedPane.setDonotRefreshChargerStatusLock(true);
				mProgressBarBootLoader.setVisible(true);
				mProgressBarBootLoader.setIndeterminate(true);
				mProgressBarBootLoader.setString(UPDATING_BOOTLOADER);
				mProgressBarBootLoader.setStringPainted(true);
				mProgressBarBootLoader.setForeground(ColorUtils.CARIBOU_GREEN);

				boolean bootLoaderStatus = ChargerDataProvider
						.doBootloaderUpdate(mBootLoaderFile);
				publish(new Progress(StringUtils.upload_bl_image.toString(),
						bootLoaderStatus));

				mProgressBarImage.setVisible(true);
				mProgressBarImage.setIndeterminate(true);
				mProgressBarImage.setString(UPDATING_IMAGELOADER_MESSAGE);
				mProgressBarImage.setStringPainted(true);
				mProgressBarImage.setForeground(ColorUtils.CARIBOU_GREEN);

				boolean imageLoderStatus = ChargerDataProvider
						.doMainImageUpdate(mImageFile);
				publish(new Progress(StringUtils.upload_main_image.toString(),
						imageLoderStatus));

				status = bootLoaderStatus && imageLoderStatus;

			} else if (!isEmpty(bootLoaderFileName)) {
				LockTabbedPane.setDonotRefreshChargerStatusLock(true);
				mProgressBarBootLoader.setVisible(true);
				mProgressBarBootLoader.setIndeterminate(true);
				mProgressBarBootLoader.setString(UPDATING_BOOTLOADER);
				mProgressBarBootLoader.setStringPainted(true);
				mProgressBarBootLoader.setForeground(ColorUtils.CARIBOU_GREEN);
				// Turn off the auto refresh
				status = ChargerDataProvider
						.doBootloaderUpdate(mBootLoaderFile);
				publish(new Progress(StringUtils.upload_bl_image.toString(),
						status));
			} else if (!isEmpty(imageLoaderFileName)) {
				LockTabbedPane.setDonotRefreshChargerStatusLock(true);
				mProgressBarImage.setVisible(true);
				mProgressBarImage.setIndeterminate(true);
				mProgressBarImage.setString(UPDATING_IMAGELOADER_MESSAGE);
				mProgressBarImage.setStringPainted(true);
				mProgressBarImage.setForeground(ColorUtils.CARIBOU_GREEN);
				status = ChargerDataProvider.doMainImageUpdate(mImageFile);
				publish(new Progress(StringUtils.upload_main_image.toString(),
						status));
			} else {
				LockTabbedPane.setDonotRefreshChargerStatusLock(false);
			}
			return status;
		}

		@Override
		protected void process(List<Progress> progressList) {
			for (Progress p : progressList) {
				if (ChargerDataProvider.DEBUG) {
					System.out.println("------ " + p.getTask() + " : "
							+ p.isComplete() + " ------");
				}
				String status = p.isComplete() ? SUCCESS_MESSAGE
						: FAILURE_MESSAGE;
				if (mProgressBarBootLoader.isShowing()) {
					mProgressBarBootLoader.setString(status);
					mProgressBarBootLoader.setIndeterminate(false);
				} else if (mProgressBarImage.isShowing()) {
					mProgressBarImage.setString(status);
					mProgressBarImage.setIndeterminate(false);
				}
			}
		}

		@Override
		protected void done() {
			try {
				String status = get() ? SUCCESS_MESSAGE : FAILURE_MESSAGE;
				if (mProgressBarBootLoader.isIndeterminate()) {
					mProgressBarBootLoader.setString(status);
					mProgressBarBootLoader.setIndeterminate(false);
				}
				if (mProgressBarImage.isIndeterminate()) {
					mProgressBarImage.setString(status);
					mProgressBarImage.setIndeterminate(false);
				}
				// Auto refresh charger status
				LockTabbedPane.setDonotRefreshChargerStatusLock(false);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

	}
}
