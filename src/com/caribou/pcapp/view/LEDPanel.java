package com.caribou.pcapp.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class LEDPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	/**
     * Create the panel.
     */

    public LEDPanel() {
        setBounds(100, 100, 880, 440);

        setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JPanel panelFirmware = new JPanel();
        panelFirmware.setBorder(new CompoundBorder(new TitledBorder(
                new LineBorder(new Color(184, 207, 229)), "LED Tests",
                TitledBorder.LEADING, TitledBorder.TOP, null,
                ColorUtils.CARIBOU_BLUE), new BevelBorder(BevelBorder.RAISED,
                new Color(255, 255, 255), null, null, null)));
        add(panelFirmware);
        panelFirmware.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JButton btnYellowButton = new JButton(new ImageIcon(LEDPanel.class.getResource("/yellowbulb32.png")));
        btnYellowButton.setToolTipText("YELLOW");
        panelFirmware.add(btnYellowButton);
        btnYellowButton.addActionListener(this);
        
        JButton btnGreenButton = new JButton(new ImageIcon(LEDPanel.class.getResource("/greenbulb32.png")));
        btnGreenButton.setToolTipText("GREEN");
        btnGreenButton.setVerticalAlignment(SwingConstants.BOTTOM);
        btnGreenButton.addActionListener(this);
        panelFirmware.add(btnGreenButton);
        
        JButton btnRedButton = new JButton(new ImageIcon(LEDPanel.class.getResource("/redbulb32.png")));
        btnRedButton.setToolTipText("RED");
        btnRedButton.addActionListener(this);
        panelFirmware.add(btnRedButton);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("***"+e.getSource().getClass().getName());
	}
}