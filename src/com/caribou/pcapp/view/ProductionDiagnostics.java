package com.caribou.pcapp.view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class ProductionDiagnostics extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTabbedPane mJtabbedPane = new JTabbedPane(JTabbedPane.TOP);

	public ProductionDiagnostics() {
		setLayout(new GridLayout(0, 1, 0, 0));
		this.add(mJtabbedPane);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new CompoundBorder(new TitledBorder(
				new LineBorder(new Color(184, 207, 229)), "Review",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51,
						51)), new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null)));
		//initializeTabs();
		//add(scrollPane);
	}

	private void initializeTabs() {
		// initialize tab 1 - Users
		// mJscrollPane = mDeviceChargingStatus.getScrollPane();
		Canvas ledCanvas = new Canvas();
		ledCanvas.setBackground(ColorUtils.CANVAS_BACKGROUND);
		mJtabbedPane.addTab(
				StringUtils.diagnosticsTab1.toString(),
				new ImageIcon(ProductionDiagnostics.class
						.getResource("/bulb16.png")), new LEDPanel(),
				StringUtils.diagnosticsTab1.toString());

		// initialize tab 2 - Lock History
		// mJcanvas = mAdminProfile.getCanvas();
		Canvas keyBoardCanvas = new Canvas();
		keyBoardCanvas.setBackground(ColorUtils.CARIBOU_BLUE);
		mJtabbedPane.addTab(
				StringUtils.diagnosticsTab2.toString(),
				new ImageIcon(ProductionDiagnostics.class
						.getResource("/keyboard24.png")), keyBoardCanvas,
				StringUtils.diagnosticsTab2.toString());

		// initialize tab 3 - Lock Management
		Canvas rtcCanvas = new Canvas();
		rtcCanvas.setBackground(ColorUtils.CARIBOU_BLUE);
		mJtabbedPane.addTab(
				StringUtils.diagnosticsTab3.toString(),
				new ImageIcon(ProductionDiagnostics.class
						.getResource("/rtc24.png")), rtcCanvas,
				StringUtils.diagnosticsTab3.toString());

		// initialize tab 4 - Diagnostics
		Canvas chargingCanvas = new Canvas();
		chargingCanvas.setBackground(ColorUtils.CARIBOU_BLUE);
		mJtabbedPane.addTab(
				StringUtils.diagnosticsTab4.toString(),
				new ImageIcon(ProductionDiagnostics.class
						.getResource("/battery.png")), chargingCanvas,
				StringUtils.diagnosticsTab4.toString());
	}

}
