package com.caribou.pcapp.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.caribou.pcapp.LockUserData;

@SuppressWarnings("serial")
public class UserDisplay extends JPanel implements ActionListener, 	TableModelListener {
	private static final String PASSWORD = "Password";
	private static final String ROLE = "Role";
	//private static final String COLUMN_EVENT = "Remove";

	public UserDisplay() {
		super(new GridLayout(1, 0));
		
		final UserTableModel model = new UserTableModel();

		final JTable table = new JTable(model);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setRowHeight(20);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			  public void mousePressed(MouseEvent e) {
			      JTable target = (JTable)e.getSource();
			      int row = target.getSelectedRow();
			      int column = target.getSelectedColumn();
			      if(column == 2){
			    	  System.out.println("row: "+row+" col: "+column);
			    	  model.fireTableRowsDeleted(row, row);
			      }
			  }
			});

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(String.class, centerRenderer);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel..
		add(scrollPane);
	}

	class UserTableModel extends AbstractTableModel {
		private String[] columnNames = { PASSWORD, ROLE };

		private Object[][] data = getHistoryData(columnNames.length);

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.length;
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		/*
		 * JTable uses this method to determine the default renderer/ editor for
		 * each cell. If we didn't implement this method, then the last column
		 * would contain text ("true"/"false"), rather than a check box.
		 */
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		/*
		 * Don't need to implement this method unless your table's editable.
		 */
		public boolean isCellEditable(int row, int col) {
			// Note that the data/cell address is constant,
			// no matter where the cell appears onscreen.
			if (col < 2) {
				return false;
			} else {
				return true;
			}
		}

		private Object[][] getHistoryData(int columCount) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// put numRows = 10 for the time being. It should be
			// lockHistory.length();

			int numRows = 30;
			Object data[][] = new Object[numRows][columCount];
			for (int i = 0; i < numRows; i++) {
				LockUserData lockUserData = new LockUserData();
				data[i][0] = lockUserData.getPasscode();
				data[i][1] = lockUserData.isAdmin() ? "Admin" : "User";
//				data[i][2] = new ImageIcon(
//						UserDisplay.class.getResource("/trash-icon16.png"));
			}
			// data[row][col] = value;
			// Normally, one should call fireTableCellUpdated() when
			// a value is changed. However, doing so in this demo
			// causes a problem with TableSorter. The tableChanged()
			// call on TableSorter that results from calling
			// fireTableCellUpdated() causes the indices to be regenerated
			// when they shouldn't be. Ideally, TableSorter should be
			// given a more intelligent tableChanged() implementation,
			// and then the following line can be uncommented.
			// fireTableCellUpdated(row, col);
			return data;
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		
	}

}
