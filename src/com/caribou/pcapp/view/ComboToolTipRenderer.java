package com.caribou.pcapp.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JList;
import javax.swing.JToolTip;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

@SuppressWarnings("serial")
public class ComboToolTipRenderer extends BasicComboBoxRenderer {
    private static String[] mProfileToolTips = new String[] {
            "Auto",
            "Apple 12W (2.4A charger)",
            "DCP and YD/T1591",
            "Blackberry, Apple low watt (0.5A), <br>LG, older cell Phones, Samsung, etc.",
            "Blackberry, older cell Phones", "Galaxy Tablets",
            "Apple 5W (1.0A charger)", "Galaxy Tablets", "BC 1.2 (DCP)",
            "BC 1.2 (CDP)" };

    // private JToolTip mJToolTip = createToolTip();

    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        if (isSelected) {
            setBackground(ColorUtils.CARIBOU_GREEN);
            setForeground(ColorUtils.CARIBOU_BLUE);
            if (-1 < index) {
                list.setToolTipText("<html><font size='4' color='rgb(84,144,204)'><b><i>"
                        + mProfileToolTips[index]);
            }
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        setFont(new Font(Font.SERIF, Font.PLAIN, 13));
        setText((value == null) ? "" : value.toString());
        return this;
    }

    @Override
    public JToolTip createToolTip() {
        JToolTip toolTip = super.createToolTip();
        toolTip.setBackground(Color.BLACK);
        toolTip.setForeground(Color.RED);

        return toolTip;
    }
}
