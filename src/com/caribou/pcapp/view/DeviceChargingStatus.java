package com.caribou.pcapp.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.caribou.pcapp.ChargerData;
import com.caribou.pcapp.ChargerDataProvider;
import com.caribou.pcapp.ChargingStatus;

public class DeviceChargingStatus {
	private JScrollPane mJscrollPane;
	private List<ChargerData> mChargerDataList = new ArrayList<ChargerData>();
	private static final String DEVICE_IS_DISCONNECTED = "Device is disconnected";
	private static final int AUTO_REFRESH_INTERVAL = 5;
	private static String[] mProfiles = new String[] { "Auto", "Legacy 1",
			"Legacy 2", "Legacy 3", "Legacy 4", "Legacy 5", "Legacy 6",
			"Legacy 7", "BC1.2 (DCP)", "BC 1.2 (CDP)" };
	private List<Object[]> mValues = new ArrayList<Object[]>();
	// Table Columns
	private static final int COL_PORT_NUM = 0;
	private static final int COL_STATION_NUM = 1;
	private static final int COL_TIME_TO_FULL_CHARGE = 2;
	private static final int COL_TIME_ON_CHARGE = 3;
	private static final int COL_CHARGING_STATUS = 4;
	private static final int COL_CHARGING_PROFILE = 5;
	private static final int COL_PROFILE_OVERRIDE = 6;

	private static final String NO_DATA = "No Data";

	public DeviceChargingStatus() {
		mJscrollPane = new JScrollPane();
		mJscrollPane.setBackground(Color.WHITE);
		getDataFromCharger();
		final JTable jTable = getJTable();
		drawTable(jTable);
		//No polling
		displayUserData(jTable);
		//refreshChargerData(jTable);
	}

	public JScrollPane getScrollPane() {
		return mJscrollPane;
	}

	private void getDataFromCharger() {
		ChargerDataProvider.init();
		mChargerDataList = ChargerDataProvider.getChargerData(0);
		initJTableValues();
	}

	private String getChargingStatus(ChargingStatus status, float current) {
		String chargingStatus = "";
		if (ChargingStatus.DISCONNECTED.equals(status)
				|| ChargingStatus.FAIL.equals(status)
				|| ChargingStatus.UNKNOWN.equals(status)
				|| ChargingStatus.CALIBRATING.equals(status)) {
			chargingStatus = status.toString();
		} else {
			chargingStatus = status + " " + current + " mA";
		}

		return chargingStatus;
	}

	private String getProfile(int profile) {
		String retVal = "";
		if (profile >= 0 && profile < 8) {
			// profile values 0 to 7 represent Auto set profiles. Adding 1 to
			// profile num since mProfiles[0] is Auto.
			retVal = mProfiles[profile + 1];
		} else if (profile >= 8 && profile <= 16) {
			// profile values 8 to 16 represent Manually set profiles.
			// Subtracting 7 since profile 8 corresponds to mProfiles[1] and so
			// on so forth.
			retVal = mProfiles[profile - 7];
		}

		return retVal;
	}

	private String getProfileType(int profile) {
		// Profile type is either Auto or one of the Manually set profiles
		String retVal = "";
		if (profile >= 0 && profile < 8) {
			// profile values 0 to 7 represent Auto set profiles.
			retVal = mProfiles[0];
		} else if (profile >= 8 && profile <= 16) {
			// profile values 8 to 16 represent Manually set profiles.
			// Subtracting 7 since profile 8 corresponds to mProfiles[1] and so
			// on so forth.
			retVal = mProfiles[profile - 7];
		}

		return retVal;
	}

	private void drawTable(JTable jTable) {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		jTable.setDefaultRenderer(Object.class, centerRenderer);
		jTable.setRowHeight(30);
		jTable.setSelectionBackground(ColorUtils.CARIBOU_BLUE);

		jTable.setShowVerticalLines(false);
		jTable.setShowGrid(false);
		jTable.setShowHorizontalLines(false);
		jTable.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null,
				null));

		JTableHeader header = jTable.getTableHeader();
		header.setBackground(ColorUtils.CARIBOU_GREEN);
		header.setForeground(Color.WHITE);
		header.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		jTable.setFont(new Font(Font.SERIF, Font.PLAIN, 14));
		resetColoumnWidth(jTable);

		// Fiddle with the Profile Overide column's cell editors/renderers.
		setUpProfileOverideColumn(jTable,
				jTable.getColumnModel().getColumn(COL_PROFILE_OVERRIDE));

		if (mChargerDataList.isEmpty()) {
			mJscrollPane.setViewportView(new JLabel(DEVICE_IS_DISCONNECTED,
					JLabel.CENTER));
			return;
		} else {
			mJscrollPane.setViewportView(jTable);
		}
	}

	private void displayUserData(final JTable jTable) {
		mJscrollPane.setViewportView(jTable);
		int rowNum = 0;
		for (ChargerData chargerData : mChargerDataList) {
			String chargingStatus = getChargingStatus(
					chargerData.getChargingStatus(), chargerData.getCurrent());
			jTable.setValueAt(chargerData.getPortNumber(), rowNum, COL_PORT_NUM);
			jTable.setValueAt(COL_STATION_NUM, rowNum, COL_STATION_NUM);
			jTable.setValueAt(chargerData.getTimeToFullCharge(), rowNum,
					COL_TIME_TO_FULL_CHARGE);
			jTable.setValueAt(chargerData.getTimeOnCharge(), rowNum,
					COL_TIME_ON_CHARGE);
			jTable.setValueAt(chargingStatus, rowNum, COL_CHARGING_STATUS);
			jTable.setValueAt(getProfile(chargerData.getChargingProfile()),
					rowNum, COL_CHARGING_PROFILE);
			String aValue = jTable.getValueAt(rowNum, COL_PROFILE_OVERRIDE)
					.toString();
			// This is to update the cell color for profile
			// override column, without telling the charger
			jTable.setValueAt("", rowNum, COL_PROFILE_OVERRIDE);
			jTable.setValueAt(aValue, rowNum, COL_PROFILE_OVERRIDE);

			rowNum++;
		}
	}

	//TODO: REMOVE LATER. KEEPING IT FOR THE TIME BEING FOR REFERENCE.
//	private void refreshChargerData(final JTable jTable) {
//		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
//				new Runnable() {
//					@Override
//					public void run() {
//						// first check if the refresh is allowed
//						if (LockTabbedPane.isStopRefresh()) {
//							if (ChargerDataProvider.DEBUG) {
//								System.out.println("------ AUTO REFRESH OFF ------");
//							}
//							return;
//						}
//						mChargerDataList = ChargerDataProvider
//								.getChargerData(0);
//						if (mChargerDataList.isEmpty()) {
//							mJscrollPane.setViewportView(new JLabel(
//									DEVICE_IS_DISCONNECTED, JLabel.CENTER));
//							return;
//						} else {
//							mJscrollPane.setViewportView(jTable);
//							int rowNum = 0;
//							for (ChargerData chargerData : mChargerDataList) {
//								String chargingStatus = getChargingStatus(
//										chargerData.getChargingStatus(),
//										chargerData.getCurrent());
//								jTable.setValueAt(chargerData.getPortNumber(),
//										rowNum, COL_PORT_NUM);
//								jTable.setValueAt(COL_STATION_NUM, rowNum,
//										COL_STATION_NUM);
//								jTable.setValueAt(
//										chargerData.getTimeToFullCharge(),
//										rowNum, COL_TIME_TO_FULL_CHARGE);
//								jTable.setValueAt(
//										chargerData.getTimeOnCharge(), rowNum,
//										COL_TIME_ON_CHARGE);
//								jTable.setValueAt(chargingStatus, rowNum,
//										COL_CHARGING_STATUS);
//								jTable.setValueAt(getProfile(chargerData
//										.getChargingProfile()), rowNum,
//										COL_CHARGING_PROFILE);
//								String aValue = jTable.getValueAt(rowNum,
//										COL_PROFILE_OVERRIDE).toString();
//								// This is to update the cell color for profile
//								// override column, without telling the charger
//								jTable.setValueAt("", rowNum,
//										COL_PROFILE_OVERRIDE);
//								jTable.setValueAt(aValue, rowNum,
//										COL_PROFILE_OVERRIDE);
//
//								rowNum++;
//							}
//						}
//					}
//				}, 0, AUTO_REFRESH_INTERVAL, TimeUnit.SECONDS);
//	}

	private void setUpProfileOverideColumn(final JTable table,
			TableColumn profileOverideColumn) {
		// Set up the editor for the profile overide cells.
		JComboBox combo = new JComboBox(mProfiles);
		combo.setRenderer(new ComboToolTipRenderer());
		profileOverideColumn.setCellEditor(new DefaultCellEditor(combo));
	}

	private void resetColoumnWidth(JTable jTable) {
		Dimension tableSize = mJscrollPane.getPreferredSize();
		jTable.getColumnModel().getColumn(0)
				.setPreferredWidth(Math.round(tableSize.width * 0.10f));
		jTable.getColumnModel().getColumn(1)
				.setPreferredWidth(Math.round(tableSize.width * 0.10f));
		jTable.getColumnModel().getColumn(2)
				.setPreferredWidth(Math.round(tableSize.width * 0.15f));
		jTable.getColumnModel().getColumn(3)
				.setPreferredWidth(Math.round(tableSize.width * 0.10f));
		jTable.getColumnModel().getColumn(4)
				.setPreferredWidth(Math.round(tableSize.width * 0.35f));
		jTable.getColumnModel().getColumn(5)
				.setPreferredWidth(Math.round(tableSize.width * 0.10f));
		jTable.getColumnModel().getColumn(6)
				.setPreferredWidth(Math.round(tableSize.width * 0.10f));
	}

	private JTable getJTable() {
		@SuppressWarnings("serial")
		final JTable jTable = new JTable(new CustomTableModel(mValues,
				mProfiles)) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer,
					int row, int column) {

				Object value = this.getModel().getValueAt(row,
						COL_CHARGING_STATUS);

				Component returnComp = super.prepareRenderer(renderer, row,
						column);
				// Set Horizontal alignment.
				((JLabel) super.prepareRenderer(renderer, row, column))
						.setHorizontalAlignment(JLabel.CENTER);

				if (!returnComp.getBackground()
						.equals(getSelectionBackground())) {
					Color bg = (row % 2 == 0 ? ColorUtils.ALTERNATE_TABLE_ROW_COLOR
							: ColorUtils.WHITE_TABLE_ROW_COLOR);
					returnComp.setBackground(bg);
					bg = null;
				}
				if (value != null) {
					if (value.toString().contains(
							ChargingStatus.DISCONNECTED.toString())) {
						returnComp.setForeground(Color.GRAY);
					} else if (value.toString().contains(
							ChargingStatus.FAIL.toString())
							|| value.toString().contains(
									ChargingStatus.UNKNOWN.toString())) {
						returnComp.setForeground(Color.RED);
					} else if (value.toString().contains(
							ChargingStatus.COMPLETE.toString())) {
						returnComp
								.setForeground(ColorUtils.CHARGING_STATUS_COMPLETE);
					} else if (value.toString().contains(
							ChargingStatus.CALIBRATING.toString())) {
						returnComp.setForeground(Color.BLUE);
					} else {
						returnComp.setForeground(Color.BLACK);
					}
				}
				return returnComp;
			}
		};
		return jTable;
	}

	private void initJTableValues() {
		if (mChargerDataList.isEmpty()) {
			for (int i = 0; i < 10; i++) {
				mValues.add(new Object[] { NO_DATA, NO_DATA, NO_DATA, NO_DATA,
						NO_DATA, NO_DATA, "" });
			}
		} else {
			for (ChargerData chargerData : mChargerDataList) {
				String chargingStatus = getChargingStatus(
						chargerData.getChargingStatus(),
						chargerData.getCurrent());
				int profile = chargerData.getChargingProfile();
				Object value = getProfile(profile);
				mValues.add(new Object[] { chargerData.getPortNumber(), 1,
						chargerData.getTimeToFullCharge(),
						chargerData.getTimeOnCharge(), chargingStatus, value,
						getProfileType(profile) });
			}
		}
	}

}
