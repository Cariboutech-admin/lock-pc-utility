package com.caribou.pcapp.view;

public enum StringUtils {
	tab1("User"), tab2("History"), tab3(
            "Management"), tab4("Diagnostics"), tableHeader0(
            "<html>Port<br>Number"), tableHeader1("<html>Station<br>Number"), tableHeader2(
            "<html>Time To<br>Full Charge"), tableHeader3(
            "<html>Time On<br>Charge"), tableHeader4("<html>Charging Status"), tableHeader5(
            "<html>Charging<br>Profile"), tableHeader6(
            "<html>Profile<br>Overide"), upload_bl_image("Upload BL image"), upload_main_image(
            "Upload main image"), update_sys_image("Update"),diagnosticsTab1("LED"),diagnosticsTab2("KeyPad"),diagnosticsTab3("RTC"),diagnosticsTab4("Charging");

    private final String name;

    private StringUtils(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    @Override
    public String toString() {
        return name;
    }
}
