package com.caribou.pcapp.view;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class LockTabbedPane {

	private JFrame mJframe = new JFrame();
	private DeviceChargingStatus mDeviceChargingStatus = new DeviceChargingStatus();
	private JScrollPane mJscrollPane;
	private JPanel mHistoryPanel = new JPanel();
	private JPanel mAdminPanel = new JPanel();
	private JPanel mUserPanel = new JPanel();
	private final String DOWNLOADING_STATUS = "Downloading";
	private JTabbedPane mJtabbedPane = new JTabbedPane(JTabbedPane.TOP);

	private volatile static boolean mStopRefresh = false;
	private static final Object mRefreshLockObj = new Object();

	public static final boolean DEBUG = true;

	public static final boolean ENABLE_DIAGNOSTICS = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// for Linux
		JFrame.setDefaultLookAndFeelDecorated(true);
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					LockTabbedPane window = new LockTabbedPane();
					window.mJframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public static void setDonotRefreshChargerStatusLock(boolean refresh) {
		synchronized (mRefreshLockObj) {
			mStopRefresh = refresh;
		}
	}

	public static boolean isStopRefresh() {
		synchronized (mRefreshLockObj) {
			return mStopRefresh;
		}
	}

	/**
	 * Create the application.
	 */
	public LockTabbedPane() {
		initializeFrame();
		initializeTabs();
	}

	private void initializeFrame() {
		URL url = LockTabbedPane.class.getResource("/favicon.ico");
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage(url);
		mJframe.setResizable(false);
		mJframe.setIconImage(img);
		mJframe.getContentPane().setBackground(Color.WHITE);
		mJframe.setVisible(true);
		mJframe.setBounds(100, 100, 880, 440);
		mJframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mJframe.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		UIManager.put("TabbedPane.selected",
				new javax.swing.plaf.ColorUIResource(ColorUtils.TAB_SELECTED));
		mJframe.getContentPane().add(mJtabbedPane);
	}

	private void initializeTabs() {
		// initialize tab 1 - Users
		// comment out this for the time being
		// mJscrollPane = mDeviceChargingStatus.getScrollPane();
		// initialize tabs
		intializeTabs();
		mJtabbedPane.addTab(StringUtils.tab3.toString(), new ImageIcon(
				LockTabbedPane.class.getResource("/lock24.png")), mAdminPanel,
				StringUtils.tab3.toString());
		mJtabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		mJtabbedPane.addTab(StringUtils.tab1.toString(), new ImageIcon(
				LockTabbedPane.class.getResource("/Admin24.png")), mUserPanel,
				StringUtils.tab1.toString());
		mJtabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

		mJtabbedPane.addTab(StringUtils.tab2.toString(), new ImageIcon(
				LockTabbedPane.class.getResource("/history32.png")),
				mHistoryPanel, StringUtils.tab2.toString()); // calling history
																// display new
																// HistoryDisplay()
		mJtabbedPane.setMnemonicAt(2, KeyEvent.VK_3);

		// initialize tab 4 - Diagnostics
		if (ENABLE_DIAGNOSTICS) {
			Canvas canvasProvisioning = new Canvas();
			canvasProvisioning.setBackground(ColorUtils.CARIBOU_BLUE);
			mJtabbedPane.addTab(StringUtils.tab4.toString(), new ImageIcon(
					LockTabbedPane.class.getResource("/Developer24.png")),
					new ProductionDiagnostics(), StringUtils.tab4.toString());
			mJtabbedPane.setMnemonicAt(3, KeyEvent.VK_4);
		}
	}

	private void intializeTabs() {
		// initialize tab 3 - Lock Management
		mJtabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				String selectedTabName = mJtabbedPane.getTitleAt(mJtabbedPane
						.getSelectedIndex());

				if (StringUtils.tab2.toString().equals(selectedTabName)) {

					mHistoryPanel.removeAll();
					final JProgressBar downloadBar = new JProgressBar();
					downloadBar.setString(DOWNLOADING_STATUS);
					downloadBar.setIndeterminate(true);
					downloadBar.setStringPainted(true);
					downloadBar.setForeground(ColorUtils.CARIBOU_BLUE);
					mHistoryPanel.setLayout(new GridBagLayout());
					GridBagConstraints gc = new GridBagConstraints();
					gc.gridy = 0;
					mHistoryPanel.setBorder(new CompoundBorder(
							new TitledBorder(new LineBorder(new Color(184, 207,
									229)), "History Data",
									TitledBorder.LEADING, TitledBorder.TOP,
									null, ColorUtils.CARIBOU_BLUE),
							new BevelBorder(BevelBorder.RAISED, new Color(255,
									255, 255), null, null, null)));
					mHistoryPanel.add(downloadBar, gc);

					SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

						@Override
						protected Void doInBackground() {
							HistoryDisplay display = new HistoryDisplay();
							mHistoryPanel.removeAll();
							mHistoryPanel.repaint();
							mHistoryPanel.setLayout(new BorderLayout());
							mHistoryPanel.add(display, BorderLayout.CENTER);
							mHistoryPanel.revalidate();
							return null;
						}

					};

					worker.execute();
				}

				if (StringUtils.tab1.toString().equals(selectedTabName)) {

					mUserPanel.removeAll();
					final JProgressBar downloadBar = new JProgressBar();
					downloadBar.setString(DOWNLOADING_STATUS);
					downloadBar.setIndeterminate(true);
					downloadBar.setStringPainted(true);
					downloadBar.setForeground(ColorUtils.CARIBOU_BLUE);
					mUserPanel.setLayout(new GridBagLayout());
					GridBagConstraints gc = new GridBagConstraints();
					gc.gridy = 0;
					mUserPanel.setBorder(new CompoundBorder(
							new TitledBorder(new LineBorder(new Color(184, 207,
									229)), "User Data",
									TitledBorder.LEADING, TitledBorder.TOP,
									null, ColorUtils.CARIBOU_BLUE),
							new BevelBorder(BevelBorder.RAISED, new Color(255,
									255, 255), null, null, null)));
					mUserPanel.add(downloadBar, gc);

					SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

						@Override
						protected Void doInBackground() {
							UserDisplay display = new UserDisplay();
							mUserPanel.removeAll();
							mUserPanel.repaint();
							mUserPanel.setLayout(new BorderLayout());
							mUserPanel.add(display, BorderLayout.CENTER);
							mUserPanel.revalidate();
							return null;
						}

					};

					worker.execute();
				}
				if (StringUtils.tab3.toString().equals(selectedTabName)) {

					mAdminPanel.removeAll();
					final JProgressBar downloadUserBar = new JProgressBar();
					downloadUserBar.setString(DOWNLOADING_STATUS);
					downloadUserBar.setIndeterminate(true);
					downloadUserBar.setStringPainted(true);
					downloadUserBar.setForeground(ColorUtils.CARIBOU_BLUE);
					mAdminPanel.setLayout(new GridBagLayout());
					GridBagConstraints gc = new GridBagConstraints();
					gc.gridy = 0;

					mAdminPanel.add(downloadUserBar, gc);

					SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

						@Override
						protected Void doInBackground() throws Exception {
							AdminProfile admin = new AdminProfile();
							mAdminPanel.removeAll();
							mAdminPanel.repaint();
							mAdminPanel.setLayout(new BorderLayout());
							mAdminPanel.add(admin, BorderLayout.CENTER);
							mAdminPanel.revalidate();
							return null;
						}

					};
					worker.execute();
				}

			}
		});
	}

}
