package com.caribou.pcapp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import org.usb4java.BufferUtils;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;

public class UsbDeviceManager {
    private static final byte INTERFACE = 0;
    private static final byte EP_IN = (byte) 0x81;
    private static final byte EP_OUT = 0x01;
    private static final int TIMEOUT = 5000; // ms

    private static boolean mIsInitialized;
    private static boolean mIsAttached;
    private static int mOpenCount;
    private static DeviceHandle mHandle;

    public static synchronized boolean write(byte[] data) {
        ByteBuffer buffer = BufferUtils.allocateByteBuffer(data.length);
        buffer.put(data);
        IntBuffer transferred = BufferUtils.allocateIntBuffer();
        int result = LibUsb.interruptTransfer(mHandle, EP_OUT, buffer,
                transferred, TIMEOUT);
        if (result != LibUsb.SUCCESS) {
            System.err.println("[write]: Unable to send data" + result);
            return false;
        }
        System.out.println(transferred.get() + " bytes sent to device");
        return true;
    }

    public static synchronized ByteBuffer read(int size) {
        ByteBuffer buffer = BufferUtils.allocateByteBuffer(size).order(
                ByteOrder.LITTLE_ENDIAN);
        IntBuffer transferred = BufferUtils.allocateIntBuffer();
        int result = LibUsb.interruptTransfer(mHandle, EP_IN, buffer,
                transferred, TIMEOUT);
        System.out.println("**** READ RESULT : " + result);
        if (result != LibUsb.SUCCESS) {
            System.err.println("[read]: Unable to read data" + result);
            return null;
        }
        System.out.println(transferred.get() + " bytes read from device");
        return buffer;
    }

    public static synchronized boolean init() {
        if (!mIsInitialized) {
            int result = LibUsb.init(null);
            if (result >= 0) {
                mIsInitialized = true;
            }
        }
        return mIsInitialized;
    }

    public static synchronized void exit() {
        LibUsb.exit(null);
        mIsInitialized = false;
    }

    public static synchronized boolean open(short vid, short pid) {
        if (mOpenCount == 0) {
            mHandle = LibUsb.openDeviceWithVidPid(null, vid, pid);
            if (mHandle == null) {
                System.err
                        .println("[UsbDeviceManager]::Unable to open the device.");
                return false;
            }

            // Check if kernel driver is attached to the interface
            int attached = LibUsb.kernelDriverActive(mHandle, INTERFACE);
            if (attached < 0) {
                mIsAttached = false;
                System.out.println("Unable to check kernel driver active: "
                        + attached);
            } else {
                mIsAttached = true;
            }
            // Detach kernel driver from interface 0 and 1. This can fail if
            // kernel is not attached to the device or operating system
            // doesn't support this operation. These cases are ignored here.
            int result = LibUsb.detachKernelDriver(mHandle, INTERFACE);
            if ((result != LibUsb.SUCCESS)
                    && (result != LibUsb.ERROR_NOT_SUPPORTED)
                    && (result != LibUsb.ERROR_NOT_FOUND)) {
                System.out.println("Unable to detach kernel driver" + result);
            }

            // Claim the interface
            result = LibUsb.claimInterface(mHandle, INTERFACE);
            if (result == LibUsb.SUCCESS) {
            } else {
                System.err.println("Unable to claim interface " + result);
            }
        }
        mOpenCount++;
        return true;
    }

    public static synchronized boolean close() {
        mOpenCount--;
        if (mOpenCount == 0) {
            // Release the ADB interface
            int result = LibUsb.releaseInterface(mHandle, INTERFACE);
            if (result != LibUsb.SUCCESS) {
                System.err.println("Unable to release interface " + result);
                return false;
            }

            if (mIsAttached) {
                LibUsb.attachKernelDriver(mHandle, 1);
                if (result != LibUsb.SUCCESS) {
                    System.err.println("Unable to re-attach kernel driver"
                            + result);
                    return false;
                }
            }

            // Close the device
            LibUsb.close(mHandle);
        }
        return true;
    }
}
