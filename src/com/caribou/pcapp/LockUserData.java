package com.caribou.pcapp;

public class LockUserData {
    private char[] mUserType = { '@', 'M', 'U' };

    public String getPasscode() {
        return LockUtils.getRandomPasscode(1, 5, 1, 5);
    }

    public boolean isAdmin() {
        return (mUserType[LockUtils.getRandomValue(2, 1)] == 'M');
    }
}
