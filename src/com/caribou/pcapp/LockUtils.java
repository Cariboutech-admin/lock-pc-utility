package com.caribou.pcapp;

public class LockUtils {
    public static int getRandomValue(int max, int min) {
        return (int) (Math.random() * max + min);
    }

    public static String getRandomPasscode(int minDigit, int maxDigit,
            int minLen, int maxLen) {
        String passcode = "";
        int len = getRandomValue(maxLen, minLen);
        for (int i = 0; i < len; i++) {
            passcode += getRandomValue(maxDigit, minDigit);
        }
        return passcode;
    }
}
