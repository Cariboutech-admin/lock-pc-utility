package com.caribou.pcapp;

public enum ChargingStatus {
    // Status values as returned by the micro-controller. Should not be changed
    // unless changed on controller's side. COMPLETE is not returned by the
    // micro-controller and is a special case.
    UNKNOWN, CONNECTED, FAIL, DISCONNECTED, COMPLETE, CALIBRATING;

    public static ChargingStatus convert(byte value) {
        if (value < 0 || value > ChargingStatus.CALIBRATING.ordinal()) {
            return ChargingStatus.UNKNOWN;
        }
        return ChargingStatus.values()[value];
    }

    public static int getMin() {
        return ChargingStatus.UNKNOWN.ordinal();
    }

    public static int getMax() {
        // COMPLETE is not considered as max since it is a special case
        return ChargingStatus.DISCONNECTED.ordinal();
    }
}
