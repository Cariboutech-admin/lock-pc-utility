package com.caribou.pcapp;

public class ChargerData {
    private int mPortNumber;
    private String mTimeOnCharge;
    private ChargingStatus mChargingStatus;
    private float mVoltage;
    private float mCurrent;
    private int mChargingProfile;
    private String mTimeToFullCharge;

    public ChargerData(int portNumber, String timeOnCharge,
            ChargingStatus chargingStatus, float voltage, float current,
            int chargingProfile, String timeToFullCharge) {
        mPortNumber = portNumber;
        mTimeOnCharge = timeOnCharge;
        mChargingStatus = chargingStatus;
        mVoltage = voltage;
        mCurrent = current;
        mChargingProfile = chargingProfile;
        mTimeToFullCharge = timeToFullCharge;
    }

    /**
     * 
     * @return Charging port number.
     */
    public int getPortNumber() {
        return mPortNumber;
    }

    /**
     * 
     * @return The time for which the device has been on charge. [TBD] the unit
     *         of time.
     */
    public String getTimeOnCharge() {
        return mTimeOnCharge;
    }

    public ChargingStatus getChargingStatus() {
        return mChargingStatus;
    }

    /**
     * 
     * @return Port voltage in Volts
     */
    public float getVoltage() {
        return mVoltage;
    }

    /**
     * 
     * @return The charging profile.
     */
    public int getChargingProfile() {
        return mChargingProfile;
    }

    /**
     * 
     * @return Current drawn by the port in milli Amps.
     */
    public float getCurrent() {
        return mCurrent;
    }

    public String getTimeToFullCharge() {
        return mTimeToFullCharge;
    }
}
