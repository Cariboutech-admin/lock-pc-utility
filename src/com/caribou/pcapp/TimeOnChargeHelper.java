package com.caribou.pcapp;

import java.util.Calendar;

public class TimeOnChargeHelper {

    // RTC data format is mmddyyyyhhmmss
    // 48 (0x30) is the ASCII value of 0 that is subtracted from each byte for
    // calculations.

    public static String getTimeOnCharge(byte[] baseRtc, byte[] currentRtc) {
        if (baseRtc == null || currentRtc == null) {
            return "0s";
        }
        Calendar calBase = Calendar.getInstance();
        calBase.set(getYear(baseRtc), getMonth(baseRtc), getDate(baseRtc),
                getHour(baseRtc), getMinute(baseRtc), getSecond(baseRtc));

        Calendar calCurr = Calendar.getInstance();
        calCurr.set(getYear(currentRtc), getMonth(currentRtc),
                getDate(currentRtc), getHour(currentRtc),
                getMinute(currentRtc), getSecond(currentRtc));

        long baseTimeInMillis = calBase.getTimeInMillis();
        long currTimeInMillis = calCurr.getTimeInMillis();

        long retVal = (currTimeInMillis - baseTimeInMillis) & 0xffffffff; // Convert
                                                                          // to
                                                                          // unsigned
                                                                          // value

        return convertToString(retVal);
    }

    private static String convertToString(long timeInMillis) {
        long seconds = (timeInMillis / 1000) % 60;
        long minutes = (timeInMillis / (1000 * 60)) % 60;
        long hours = (timeInMillis / (1000 * 60 * 60)) % 24;
        long days = timeInMillis / (1000 * 60 * 60 * 24);

        String ret = "";
        if (days > 0) {
            ret += days + "d ";
        }
        if (days > 0 || hours > 0) {
            ret += hours + "h ";
        }
        if (days > 0 || hours > 0 || minutes > 0) {
            ret += minutes + "m ";
        }
        ret += seconds + "s";

        return ret;
    }

    private static int getMonth(byte[] rtc) {
        return (((rtc[0] - 48) * 10) + rtc[1] - 48);
    }

    private static int getDate(byte[] rtc) {
        return (((rtc[2] - 48) * 10) + rtc[3] - 48);
    }

    private static int getYear(byte[] rtc) {
        return (((rtc[4] - 48) * 1000) + ((rtc[5] - 48) * 100)
                + ((rtc[6] - 48) * 10) + rtc[7] - 48);
    }

    private static int getHour(byte[] rtc) {
        return (((rtc[8] - 48) * 10) + rtc[9] - 48);
    }

    private static int getMinute(byte[] rtc) {
        return (((rtc[10] - 48) * 10) + rtc[11] - 48);
    }

    private static int getSecond(byte[] rtc) {
        return (((rtc[12] - 48) * 10) + rtc[13] - 48);
    }
}
